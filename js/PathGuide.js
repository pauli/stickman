function PathGuide(d, h, a, c, p, m) {
    var b = d;
    var j = a;
    var g = c;
    var n = p;
    var i = h;
    var o = 1;
    var f;
    var e = m;
    var l = false;
    var k = 0;
    this.animateFrame = function (q) {
        for (var r = 0; r < q; r++) {
            if (k > i || l == true) {
            } else {
                if (e[k] != true) {
                } else {
                    if (f != null) {
                        var u = j[k];
                        var s = g[k];
                        var t = n[k];
                        if (u != null || s != null) {
                            if (u == null) {
                                u = 0;
                            }
                            if (s == null) {
                                s = 0;
                            }
                            f.translate(u, s);
                            f.draw(true);
                        }
                        if (t != null && t != o) {
                            if (t == 0) {
                                f.clearDisplay();
                            }
                            o = t;
                        }
                    }
                }
                k++;
            }
        }
    };
    this.pauseAnimation = function () {
        l = true;
    };
    this.resumeAnimation = function () {
        l = false;
    };
    this.setPathGroup = function (q) {
        f = q;
    };
}
