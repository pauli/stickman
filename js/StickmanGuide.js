function StickmanGuide(Q, P, E, D, G, F, I, H, M, K, v, u, y, x, m, k) {
    var n;
    var N;
    var w = Q;
    var j = P;
    var S = E;
    var R = D;
    var b = G;
    var a = F;
    var d = I;
    var c = H;
    var g = M;
    var e = K;
    var p = v;
    var o = u;
    var i = y;
    var f = x;
    var C = m;
    var B = k;
    var l = false;
    var q;
    var r;
    var L = 0;
    var J = 0;
    var s = false;
    var O = false;
    var A = 0;
    this.animateFrame = function (ab) {
        for (var ae = 0; ae < ab; ae++) {
            if (A <= j && O == false) {
                if (N == null || n == null) {
                    A++;
                } else {
                    var an = S[A];
                    if (an != null) {
                        n.scaleTLx(Number(an) + 1);
                    }
                    var ak = R[A];
                    if (ak != null) {
                        n.scaleTLy(Number(ak) + 1);
                    }
                    var Y = b[A];
                    if (Y != null) {
                        n.scaleTRx(Number(Y) + 1);
                    }
                    var V = a[A];
                    if (V != null) {
                        n.scaleTRy(Number(V) + 1);
                    }
                    var X = d[A];
                    if (X != null) {
                        n.scaleBLx(Number(X) + 1);
                    }
                    var U = c[A];
                    if (U != null) {
                        n.scaleBLy(Number(U) + 1);
                    }
                    var aa = g[A];
                    if (aa != null) {
                        n.scaleBRx(Number(aa) + 1);
                    }
                    var Z = e[A];
                    if (Z != null) {
                        n.scaleBRy(Number(Z) + 1);
                    }
                    var ah = i[A];
                    var ag = f[A];
                    var T = C[A];
                    var am = B[A];
                    if (ah != null) {
                        N.setScale(ah, ag, T, am);
                    }
                    var al = p[A];
                    var aj = o[A];
                    if (al != null && aj != null) {
                        if (s == true) {
                            var ad = N.getPathGroup();
                            if (L == 0) {
                                L = al;
                                w.swirlingStarted();
                            }
                            if (J == 0) {
                                J = aj;
                            }
                            ad.translateLockedPos(al - L, aj - J);
                            L = al;
                            J = aj;
                        } else {
                            if (l == true) {
                                var ai = N.getFeetPosition();
                                var W = q.getCenterPos();
                                l = false;
                                r = new Point(Number(al), Number(aj));
                                var ac = Number(aj) + ai.y - W.y;
                                if (ac < Number(aj) + 150) {
                                    ac = Number(aj) + 150;
                                }
                                N.setDestination(Number(al) + ai.x - W.x, ac, z, true);
                            } else {
                                N.setDestination(Number(al), Number(aj), h);
                            }
                            w.pauseAnimation();
                        }
                    }
                    A++;
                }
            }
            if (A > j) {
                var af = true;
            }
        }
        if (N != null) {
            N.animateFrame(ab);
        }
    };
    this.pauseAnimation = function () {
        O = true;
    };
    this.resumeAnimation = function () {
        O = false;
    };
    var h = function () {
        w.resumeAnimation();
    };
    var z = function () {
        N.reachItemTowardPoint(q, r, t);
    };
    var t = function () {
        N.stopHoldingItem(q);
        setTimeout(q.clearDisplay, 200);
        w.resumeAnimation();
    };
    this.setStickman = function (T) {
        N = T;
        n = N.getDistortion();
    };
    this.setNextWalkToUnlock = function (T) {
        q = T;
        l = true;
    };
    this.setSwirling = function (T) {
        s = T;
    };
}
