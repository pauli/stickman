var paper;
var screenWidth = 900;
var screenHeight = 900;
var anim;
var loader;

$(document).ready(function () {
    paper = Raphael('holder', 980, 680);
    anim = new XMLAnim(paper, 'animguides.xml');
    anim.loadAnimation();
    loader = paper.image('img/loader.gif', 507 - 51, 250, 103, 91);

    var agent = navigator.userAgent.toLowerCase();
    var is_iphone = agent.indexOf('iphone') != -1;
    var is_ipad = navigator.userAgent.match(/iPad/i) != null;

    //$("#firstLine").change(setupShareLinks);
    //$("#secLine").change(setupShareLinks);

    $('#email').click(function () {
        if ($(this).val() == 'Email') {
            $(this).val('');
        }
    });

    var AJAX = 0;

    $('.submitButton').click(function () {
        if (!AJAX) {
            AJAX = 1;
            $('#ajax-wait').show();
            $.ajax({
                type: 'POST',
                url: '/email_submit.php',
                data: 'email=' + $('#email').val(),
                dataType: 'json',
                success: function (data) {
                    AJAX = 0;
                    if (data.valid == 1) {
                        $('.emailSignup div:first').html("<span style='color: #E46419;'>" + data.message + '</span>');
                        $('.emailSignup input').remove();
                        $('.emailSignup a').remove();
                    } else {
                        alert(data.message);
                    }
                    $('#ajax-wait').hide();
                },
                error: function (msg) {
                    alert('We encountered an unknown error. Please try again!');
                    AJAX = 0;
                    $('#ajax-wait').hide();
                },
            });
        }

        return false;
    });
});

function deleteLoader() {
    loader.remove();
}

function resizeWindow(e) {
    var newWindowWidth = $(window).width();
    var newWindowHeight = $(window).height();
}

function setupShareLinks() {}

function replay() {
    window.location = '/draw-a-stickman';
}

function formatForQuery() {
    var firstLineStr = $('#firstLine').val();
    var secLineStr = $('#secLine').val();
    if (firstLineStr == '' && secLineStr == '') {
        firstLineStr = 'BE CREATIVE';
        secLineStr = 'EVERY DAY';
    } else if (firstLineStr == '') {
        firstLineStr = ' ';
    } else if (secLineStr == '') {
        secLineStr = ' ';
    }

    var firstLineCode = convertString(firstLineStr);
    var secLineCode = convertString(secLineStr);

    var urlAr = new Array('index.htm?o=', firstLineCode, 's', secLineCode);
    var urlStr = urlAr.join('');
    return urlStr;
}

function convertString(str) {
    var numChars = typeof str == 'undefined' ? 0 : str.length;
    var codes = new Array();
    for (var i = 0; i < numChars; i++) {
        var charCode = str.charCodeAt(i);
        codes.push(charCode);
        if (i < numChars - 1) {
            codes.push('-');
        }
    }
    var codeStr = codes.join('');
    return codeStr;
}

function scrollDownOverlay() {
    $('#overlay').animate({ top: '500' }, { duration: 1500 }, function () {});
}

function scrollDownGalleryInfo() {
    $('#galleryInfoBox').css('top', '720px');
    $('#galleryInfoBox').animate({ top: '915' }, { duration: 1000 }, function () {});
}

function scrollUpOverlay() {
    $('#galleryInfoBox').animate({ top: '-500' }, { duration: 1000 }, function () {});
    $('#overlay').animate({ top: '-400' }, { duration: 1500 }, function () {});
}

$(window).bind('resize', resizeWindow);
