function Stickman(d, b) {
    var H;
    var j;
    var q = d;
    var n;
    var m;
    var t;
    var s;
    var y;
    var w;
    var B;
    var A;
    var i;
    var g;
    var l;
    var k;
    var p;
    var o;
    var v;
    var u;
    var a = b;
    var O;
    var N;
    var C;
    var f = false;
    var h;
    var e = 0;
    var r = 1;
    var E = 3;
    var x = 4;
    var L = 2;
    var K = 5;
    var G = 6;
    var D = e;
    var c = false;
    var F;
    q.lockRightFoot();
    this.reachItemTowardPoint = function (Z, T, S) {
        h = S;
        if (Z.getCenterPosWithOffset().x <= q.getFeetPosition().x) {
            var U = 0;
        } else {
            U = 1;
        }
        if (U == 0) {
            var P = q.makeReachLeftDistortion(Z, T);
        } else {
            var P = q.makeReachRightDistortion(Z, T);
        }
        var Q = q.getDistortion();
        var Y = 8;
        var X = 3;
        var W = 3;
        var V = 8;
        n = new Animation(Q.getTLScaleX);
        m = new Animation(Q.getTLScaleY);
        t = new Animation(Q.getTRScaleX);
        s = new Animation(Q.getTRScaleY);
        y = new Animation(Q.getBLScaleX);
        w = new Animation(Q.getBLScaleY);
        B = new Animation(Q.getBRScaleX);
        A = new Animation(Q.getBRScaleY);
        if (U == 0) {
            n.addTween(new Tween(Q.scaleTLx, Y, P.x));
            m.addTween(new Tween(Q.scaleTLy, Y, P.y));
            t.addTween(new Tween(Q.scaleTRx, Y, 1));
            s.addTween(new Tween(Q.scaleTRy, Y, 1));
            n.addTween(new Tween(Q.scaleTLx, X + W, P.x));
            m.addTween(new Tween(Q.scaleTLy, X + W, P.y));
            t.addTween(new Tween(Q.scaleTRx, X, 1));
            s.addTween(new Tween(Q.scaleTRy, X, 1));
            n.addTween(new Tween(Q.scaleTLx, V, 1));
            m.addTween(new Tween(Q.scaleTLy, V, 1));
            t.addTween(new Tween(Q.scaleTRx, V, 1));
            s.addTween(new Tween(Q.scaleTRy, V, 1));
        } else {
            n.addTween(new Tween(Q.scaleTLx, Y, 1));
            m.addTween(new Tween(Q.scaleTLy, Y, 1));
            t.addTween(new Tween(Q.scaleTRx, Y, P.x));
            s.addTween(new Tween(Q.scaleTRy, Y, P.y));
            n.addTween(new Tween(Q.scaleTLx, X + W, 1));
            m.addTween(new Tween(Q.scaleTLy, X + W, 1));
            t.addTween(new Tween(Q.scaleTRx, X + W, P.x));
            s.addTween(new Tween(Q.scaleTRy, X + W, P.y));
            n.addTween(new Tween(Q.scaleTLx, V, 1));
            m.addTween(new Tween(Q.scaleTLy, V, 1));
            t.addTween(new Tween(Q.scaleTRx, V, 1));
            s.addTween(new Tween(Q.scaleTRy, V, 1));
        }
        y.addTween(new Tween(Q.scaleBLx, Y + X + W + V, 1));
        w.addTween(new Tween(Q.scaleBLy, Y + X + W + V, 1));
        B.addTween(new Tween(Q.scaleBRx, Y + X + W + V, 1));
        A.addTween(new Tween(Q.scaleBRy, Y + X + W + V, 1));
        var R = Z.getDistortion();
        i = new Animation(R.getTLScaleX);
        g = new Animation(R.getTLScaleY);
        l = new Animation(R.getTRScaleX);
        k = new Animation(R.getTRScaleY);
        p = new Animation(R.getBLScaleX);
        o = new Animation(R.getBLScaleY);
        v = new Animation(R.getBRScaleX);
        u = new Animation(R.getBRScaleY);
        i.addTween(new Tween(R.scaleTLx, Y, 1));
        g.addTween(new Tween(R.scaleTLy, Y, 1));
        l.addTween(new Tween(R.scaleTRx, Y, 1));
        k.addTween(new Tween(R.scaleTRy, Y, 1));
        p.addTween(new Tween(R.scaleBLx, Y, 1));
        o.addTween(new Tween(R.scaleBLy, Y, 1));
        v.addTween(new Tween(R.scaleBRx, Y, 1));
        u.addTween(new Tween(R.scaleBRy, Y, 1));
        i.addTween(new Tween(R.scaleTLx, X, 0.5));
        g.addTween(new Tween(R.scaleTLy, X, 0.5));
        l.addTween(new Tween(R.scaleTRx, X, 1));
        k.addTween(new Tween(R.scaleTRy, X, 1));
        p.addTween(new Tween(R.scaleBLx, X, 0.5));
        o.addTween(new Tween(R.scaleBLy, X, 0.5));
        v.addTween(new Tween(R.scaleBRx, X, 1));
        u.addTween(new Tween(R.scaleBRy, X, 1));
        i.addTween(new Tween(R.scaleTLx, W, 1));
        g.addTween(new Tween(R.scaleTLy, W, 1));
        l.addTween(new Tween(R.scaleTRx, W, 1));
        k.addTween(new Tween(R.scaleTRy, W, 1));
        p.addTween(new Tween(R.scaleBLx, W, 1));
        o.addTween(new Tween(R.scaleBLy, W, 1));
        v.addTween(new Tween(R.scaleBRx, W, 1));
        u.addTween(new Tween(R.scaleBRy, W, 1));
        n.callFunctionAt(J, Y + X + W + V);
        newDir = G;
        D = G;
        c = true;
    };
    this.walkRight = function () {
        var Q = q.getDistortion();
        var U = 6;
        var T = 3;
        var S = 4;
        var R = 4;
        var P = 4;
        n = new Animation(Q.getTLScaleX);
        m = new Animation(Q.getTLScaleY);
        t = new Animation(Q.getTRScaleX);
        s = new Animation(Q.getTRScaleY);
        y = new Animation(Q.getBLScaleX);
        w = new Animation(Q.getBLScaleY);
        B = new Animation(Q.getBRScaleX);
        A = new Animation(Q.getBRScaleY);
        n.addTween(new Tween(Q.scaleTLx, U, 1.13));
        m.addTween(new Tween(Q.scaleTLy, U, 0.8));
        t.addTween(new Tween(Q.scaleTRx, U, 1.1));
        s.addTween(new Tween(Q.scaleTRy, U, 0.8));
        n.addTween(new Tween(Q.scaleTLx, T, 0.7));
        m.addTween(new Tween(Q.scaleTLy, T, 1));
        t.addTween(new Tween(Q.scaleTRx, T, 1.3));
        s.addTween(new Tween(Q.scaleTRy, T, 0.9));
        n.addTween(new Tween(Q.scaleTLx, S, 0.6));
        m.addTween(new Tween(Q.scaleTLy, S, 0.9));
        t.addTween(new Tween(Q.scaleTRx, S, 1.4));
        s.addTween(new Tween(Q.scaleTRy, S, 0.8));
        n.addTween(new Tween(Q.scaleTLx, R, 0.9));
        m.addTween(new Tween(Q.scaleTLy, R, 0.95));
        t.addTween(new Tween(Q.scaleTRx, R, 1.1));
        s.addTween(new Tween(Q.scaleTRy, R, 1));
        n.addTween(new Tween(Q.scaleTLx, P, 0.8));
        m.addTween(new Tween(Q.scaleTLy, P, 0.9));
        t.addTween(new Tween(Q.scaleTRx, P, 1.2));
        s.addTween(new Tween(Q.scaleTRy, P, 0.8));
        y.addTween(new Tween(Q.scaleBLx, U, 1));
        w.addTween(new Tween(Q.scaleBLy, U, 1));
        B.addTween(new Tween(Q.scaleBRx, U, 1));
        A.addTween(new Tween(Q.scaleBRy, U, 1));
        y.addTween(new Tween(Q.scaleBLx, T, 1));
        w.addTween(new Tween(Q.scaleBLy, T, 1));
        B.addTween(new Tween(Q.scaleBRx, T, 1.3));
        A.addTween(new Tween(Q.scaleBRy, T, 0.75));
        y.addTween(new Tween(Q.scaleBLx, S, 1));
        w.addTween(new Tween(Q.scaleBLy, S, 1));
        B.addTween(new Tween(Q.scaleBRx, S, 1.5));
        A.addTween(new Tween(Q.scaleBRy, S, 0.9));
        y.addTween(new Tween(Q.scaleBLx, R, 0.65));
        w.addTween(new Tween(Q.scaleBLy, R, 0.85));
        B.addTween(new Tween(Q.scaleBRx, R, 1));
        A.addTween(new Tween(Q.scaleBRy, R, 0.9));
        y.addTween(new Tween(Q.scaleBLx, P, 0.6));
        w.addTween(new Tween(Q.scaleBLy, P, 1));
        B.addTween(new Tween(Q.scaleBRx, P, 1));
        A.addTween(new Tween(Q.scaleBRy, P, 0.9));
        n.setLoop(1);
        m.setLoop(1);
        t.setLoop(1);
        s.setLoop(1);
        y.setLoop(1);
        w.setLoop(1);
        B.setLoop(1);
        A.setLoop(1);
        n.callFunctionAt(q.lockLeftFoot, 1);
        n.callFunctionAt(q.lockRightFoot, U + T + S);
        n.callFunctionAt(q.lockLeftFoot, U);
        D = L;
    };
    this.walkLeft = function () {
        var Q = q.getDistortion();
        var U = 6;
        var T = 3;
        var S = 4;
        var R = 4;
        var P = 4;
        n = new Animation(Q.getTLScaleX);
        m = new Animation(Q.getTLScaleY);
        t = new Animation(Q.getTRScaleX);
        s = new Animation(Q.getTRScaleY);
        y = new Animation(Q.getBLScaleX);
        w = new Animation(Q.getBLScaleY);
        B = new Animation(Q.getBRScaleX);
        A = new Animation(Q.getBRScaleY);
        t.addTween(new Tween(Q.scaleTRx, U, 1.13));
        s.addTween(new Tween(Q.scaleTRy, U, 0.8));
        n.addTween(new Tween(Q.scaleTLx, U, 1.1));
        m.addTween(new Tween(Q.scaleTLy, U, 0.8));
        t.addTween(new Tween(Q.scaleTRx, T, 0.7));
        s.addTween(new Tween(Q.scaleTRy, T, 1));
        n.addTween(new Tween(Q.scaleTLx, T, 1.3));
        m.addTween(new Tween(Q.scaleTLy, T, 0.9));
        t.addTween(new Tween(Q.scaleTRx, S, 0.6));
        s.addTween(new Tween(Q.scaleTRy, S, 0.9));
        n.addTween(new Tween(Q.scaleTLx, S, 1.4));
        m.addTween(new Tween(Q.scaleTLy, S, 0.8));
        t.addTween(new Tween(Q.scaleTRx, R, 0.9));
        s.addTween(new Tween(Q.scaleTRy, R, 0.95));
        n.addTween(new Tween(Q.scaleTLx, R, 1.1));
        m.addTween(new Tween(Q.scaleTLy, R, 1));
        t.addTween(new Tween(Q.scaleTRx, P, 0.8));
        s.addTween(new Tween(Q.scaleTRy, P, 0.9));
        n.addTween(new Tween(Q.scaleTLx, P, 1.2));
        m.addTween(new Tween(Q.scaleTLy, P, 0.8));
        B.addTween(new Tween(Q.scaleBRx, U, 1));
        A.addTween(new Tween(Q.scaleBRy, U, 1));
        y.addTween(new Tween(Q.scaleBLx, U, 1));
        w.addTween(new Tween(Q.scaleBLy, U, 1));
        B.addTween(new Tween(Q.scaleBRx, T, 1));
        A.addTween(new Tween(Q.scaleBRy, T, 1));
        y.addTween(new Tween(Q.scaleBLx, T, 1.3));
        w.addTween(new Tween(Q.scaleBLy, T, 0.75));
        B.addTween(new Tween(Q.scaleBRx, S, 1));
        A.addTween(new Tween(Q.scaleBRy, S, 1));
        y.addTween(new Tween(Q.scaleBLx, S, 1.5));
        w.addTween(new Tween(Q.scaleBLy, S, 0.9));
        B.addTween(new Tween(Q.scaleBRx, R, 0.65));
        A.addTween(new Tween(Q.scaleBRy, R, 0.85));
        y.addTween(new Tween(Q.scaleBLx, R, 1));
        w.addTween(new Tween(Q.scaleBLy, R, 1));
        B.addTween(new Tween(Q.scaleBRx, P, 0.6));
        A.addTween(new Tween(Q.scaleBRy, P, 1));
        y.addTween(new Tween(Q.scaleBLx, P, 1));
        w.addTween(new Tween(Q.scaleBLy, P, 0.9));
        n.setLoop(1);
        m.setLoop(1);
        t.setLoop(1);
        s.setLoop(1);
        y.setLoop(1);
        w.setLoop(1);
        B.setLoop(1);
        A.setLoop(1);
        n.callFunctionAt(q.lockRightFoot, 1);
        n.callFunctionAt(q.lockLeftFoot, U + T + S);
        n.callFunctionAt(q.lockRightFoot, U);
        D = x;
    };
    this.walkDown = function () {
        var P = q.getDistortion();
        var T = 3;
        var S = 5;
        var R = 3;
        var Q = 5;
        n = new Animation(P.getTLScaleX);
        m = new Animation(P.getTLScaleY);
        t = new Animation(P.getTRScaleX);
        s = new Animation(P.getTRScaleY);
        y = new Animation(P.getBLScaleX);
        w = new Animation(P.getBLScaleY);
        B = new Animation(P.getBRScaleX);
        A = new Animation(P.getBRScaleY);
        t.addTween(new Tween(P.scaleTRx, T, 1.01));
        s.addTween(new Tween(P.scaleTRy, T, 1.05));
        n.addTween(new Tween(P.scaleTLx, T, 0.99));
        m.addTween(new Tween(P.scaleTLy, T, 1.1));
        t.addTween(new Tween(P.scaleTRx, S, 0.98));
        s.addTween(new Tween(P.scaleTRy, S, 0.7));
        n.addTween(new Tween(P.scaleTLx, S, 1.01));
        m.addTween(new Tween(P.scaleTLy, S, 0.85));
        t.addTween(new Tween(P.scaleTRx, R, 0.94));
        s.addTween(new Tween(P.scaleTRy, R, 1.1));
        n.addTween(new Tween(P.scaleTLx, R, 1.01));
        m.addTween(new Tween(P.scaleTLy, R, 1.02));
        t.addTween(new Tween(P.scaleTRx, Q, 1.01));
        s.addTween(new Tween(P.scaleTRy, Q, 0.85));
        n.addTween(new Tween(P.scaleTLx, Q, 0.99));
        m.addTween(new Tween(P.scaleTLy, Q, 0.7));
        B.addTween(new Tween(P.scaleBRx, T, 1));
        A.addTween(new Tween(P.scaleBRy, T, 1.1));
        y.addTween(new Tween(P.scaleBLx, T, 1));
        w.addTween(new Tween(P.scaleBLy, T, 0.8));
        B.addTween(new Tween(P.scaleBRx, S, 1));
        A.addTween(new Tween(P.scaleBRy, S, 0.9));
        y.addTween(new Tween(P.scaleBLx, S, 1));
        w.addTween(new Tween(P.scaleBLy, S, 1.3));
        B.addTween(new Tween(P.scaleBRx, R, 1));
        A.addTween(new Tween(P.scaleBRy, R, 0.8));
        y.addTween(new Tween(P.scaleBLx, R, 1));
        w.addTween(new Tween(P.scaleBLy, R, 1.1));
        B.addTween(new Tween(P.scaleBRx, Q, 1));
        A.addTween(new Tween(P.scaleBRy, Q, 1.3));
        y.addTween(new Tween(P.scaleBLx, Q, 1));
        w.addTween(new Tween(P.scaleBLy, Q, 0.9));
        n.setLoop(0);
        m.setLoop(0);
        t.setLoop(0);
        s.setLoop(0);
        y.setLoop(0);
        w.setLoop(0);
        B.setLoop(0);
        A.setLoop(0);
        n.callFunctionAt(q.lockRightFoot, 1);
        n.callFunctionAt(q.lockLeftFoot, T + S);
        D = E;
    };
    this.walkUp = function () {
        var P = q.getDistortion();
        var T = 5;
        var S = 5;
        var R = 5;
        var Q = 5;
        n = new Animation(P.getTLScaleX);
        m = new Animation(P.getTLScaleY);
        t = new Animation(P.getTRScaleX);
        s = new Animation(P.getTRScaleY);
        y = new Animation(P.getBLScaleX);
        w = new Animation(P.getBLScaleY);
        B = new Animation(P.getBRScaleX);
        A = new Animation(P.getBRScaleY);
        t.addTween(new Tween(P.scaleTRx, T, 1.05));
        s.addTween(new Tween(P.scaleTRy, T, 1.1));
        n.addTween(new Tween(P.scaleTLx, T, 0.8));
        m.addTween(new Tween(P.scaleTLy, T, 1.05));
        t.addTween(new Tween(P.scaleTRx, S, 0.8));
        s.addTween(new Tween(P.scaleTRy, S, 1));
        n.addTween(new Tween(P.scaleTLx, S, 1.1));
        m.addTween(new Tween(P.scaleTLy, S, 0.8));
        t.addTween(new Tween(P.scaleTRx, R, 0.8));
        s.addTween(new Tween(P.scaleTRy, R, 1.05));
        n.addTween(new Tween(P.scaleTLx, R, 1.05));
        m.addTween(new Tween(P.scaleTLy, R, 1.1));
        t.addTween(new Tween(P.scaleTRx, Q, 1.1));
        s.addTween(new Tween(P.scaleTRy, Q, 0.8));
        n.addTween(new Tween(P.scaleTLx, Q, 0.8));
        m.addTween(new Tween(P.scaleTLy, Q, 1));
        B.addTween(new Tween(P.scaleBRx, T, 1));
        A.addTween(new Tween(P.scaleBRy, T, 1));
        y.addTween(new Tween(P.scaleBLx, T, 1.2));
        w.addTween(new Tween(P.scaleBLy, T, 0.7));
        B.addTween(new Tween(P.scaleBRx, S, 1));
        A.addTween(new Tween(P.scaleBRy, S, 1));
        y.addTween(new Tween(P.scaleBLx, S, 1));
        w.addTween(new Tween(P.scaleBLy, S, 0.8));
        B.addTween(new Tween(P.scaleBRx, R, 1.2));
        A.addTween(new Tween(P.scaleBRy, R, 0.7));
        y.addTween(new Tween(P.scaleBLx, R, 1));
        w.addTween(new Tween(P.scaleBLy, R, 1));
        B.addTween(new Tween(P.scaleBRx, Q, 1));
        A.addTween(new Tween(P.scaleBRy, Q, 0.8));
        y.addTween(new Tween(P.scaleBLx, Q, 1));
        w.addTween(new Tween(P.scaleBLy, Q, 1));
        n.setLoop(0);
        m.setLoop(0);
        t.setLoop(0);
        s.setLoop(0);
        y.setLoop(0);
        w.setLoop(0);
        B.setLoop(0);
        A.setLoop(0);
        n.callFunctionAt(q.lockRightFoot, 1);
        n.callFunctionAt(q.lockLeftFoot, T + S);
        D = r;
    };
    this.stoppingAnim = function () {
        var P = q.getDistortion();
        var Q = 5;
        n = new Animation(P.getTLScaleX);
        m = new Animation(P.getTLScaleY);
        t = new Animation(P.getTRScaleX);
        s = new Animation(P.getTRScaleY);
        y = new Animation(P.getBLScaleX);
        w = new Animation(P.getBLScaleY);
        B = new Animation(P.getBRScaleX);
        A = new Animation(P.getBRScaleY);
        t.addTween(new Tween(P.scaleTRx, Q, 1));
        s.addTween(new Tween(P.scaleTRy, Q, 1));
        n.addTween(new Tween(P.scaleTLx, Q, 1));
        m.addTween(new Tween(P.scaleTLy, Q, 1));
        B.addTween(new Tween(P.scaleBRx, Q, 1));
        A.addTween(new Tween(P.scaleBRy, Q, 1));
        y.addTween(new Tween(P.scaleBLx, Q, 1));
        w.addTween(new Tween(P.scaleBLy, Q, 1));
        n.callFunctionAt(q.lockRightFoot, 1);
        n.callFunctionAt(J, 6);
        D = K;
    };
    var z = new Array(this.stopAnim, this.walkUp, this.walkRight, this.walkDown, this.walkLeft, this.stoppingAnim, this.reachItemTowardPoint);
    var M = function () {
        if (D == G) {
            return D;
        }
        var U = D;
        var P = q.getFeetPosition();
        var T = Math.round(O - P.x);
        var R = Math.round(N - P.y);
        var S = 10;
        var Q = 40;
        if (D == e && f == true) {
            if (R > S && F == true) {
                U = E;
            } else {
                if (R < -S && F == true) {
                    U = r;
                } else {
                    if (T > S) {
                        U = L;
                    } else {
                        if (T < -S) {
                            U = x;
                        }
                    }
                }
            }
            f = false;
        } else {
            if ((D == L && T < 0) || (D == x && T > 0)) {
                if (R > Q) {
                    U = E;
                } else {
                    if (R < -Q) {
                        U = r;
                    } else {
                        U = K;
                    }
                }
            } else {
                if ((D == r && R > 0) || (D == E && R < 0)) {
                    if (T > Q) {
                        U = L;
                    } else {
                        if (T < -Q) {
                            U = x;
                        } else {
                            U = K;
                        }
                    }
                }
            }
        }
        F = false;
        return U;
    };
    var J = function () {
        newDir = e;
        D = e;
        c = false;
        h();
    };
    this.setDestination = function (R, Q, P, S) {
        O = R;
        N = Q;
        f = true;
        D = e;
        newDir = e;
        h = P;
        if (C != null) {
            C.remove();
        }
        c = true;
        F = S;
    };
    this.animateFrame = function (P) {
        for (var Q = 0; Q < P; Q++) {
            if (c == true) {
                var R = M();
                if (R != D) {
                    z[R]();
                }
                n.animate();
                m.animate();
                t.animate();
                s.animate();
                y.animate();
                w.animate();
                B.animate();
                A.animate();
                if (D == G) {
                    i.animate();
                    g.animate();
                    l.animate();
                    k.animate();
                    p.animate();
                    o.animate();
                    v.animate();
                    u.animate();
                }
            }
        }
        q.draw();
    };
    var I = function () {
        c = true;
    };
    this.getDistortion = function () {
        return q.getDistortion();
    };
    this.setScale = function (S, R, Q, P) {
        q.setScale(S, R, Q, P);
    };
    this.getTopPos = function () {
        return q.getTopPos();
    };
    this.holdItem = function (P) {
        q.holdPathGroup(P);
    };
    this.stopHoldingItem = function (P) {
        q.stopHoldingItem(P);
    };
    this.getFeetPosition = function () {
        return q.getFeetPosition();
    };
    this.getPathGroup = function () {
        return q;
    };
}
