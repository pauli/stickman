function XMLAnim(f, o) {
    var b = f;
    var d = o;
    var u = new Array();
    var p = 0;
    var x = 0;
    var aa = 0;
    var M = new Array();
    var X = new Array();
    var Z = 0;
    var e = 0;
    var F = this;
    var R;
    var A;
    var z;
    var W;
    var L;
    var E;
    var T;
    var y;
    var q;
    var G = false;
    var h = false;
    var P = 33;
    var H;
    var O = 1;
    var l;
    var S;
    var k = false;
    var K = 0;
    var U = false;
    var C = 'Dream It';
    var N = 'Real';
    var g = new Array();
    var B = new Array();
    var Y = 17;
    var w = 14;
    var I = false;
    var s = 0;
    var c = 0;
    var r = false;
    var a = false;
    var m = function () {
        x++;
        if (x >= p) {
            t();
        }
    };
    var t = function () {
        if (G == true) {
            return;
        }
        var ah = new Date().getTime();
        I = false;
        for (var ag = 0; ag < O; ag++) {
            z.animateFrame(1);
            s++;
            for (ac = 0; ac < aa; ac++) {
                M[ac].animateFrame(1);
            }
            if (r == true || a == true) {
                O = ag + 1;
                break;
            }
        }
        c += O;
        for (var ac = 0; ac < p; ac++) {
            u[ac].animateFrame(O);
        }
        for (ac = 0; ac < Z; ac++) {
            X[ac].animateFrame(O);
        }
        if (h == true) {
            var af = egg.animateEgg(O);
            if (af == true) {
                h = false;
            }
        }
        if (k == true) {
            l.animateRain(O);
        }
        if (r == true) {
            j();
        }
        if (a == true) {
            v();
        }
        var ae = new Date().getTime() - ah;
        var ab = P - ae;
        if (ab < 0) {
            O = Math.round(-ab / P) + 2;
            if (O > 5) {
                O = 5;
            }
        } else {
            O = 1;
        }
        if (ab <= 0) {
            ab = 1;
        }
        if (U == true) {
            K++;
            if (K >= 260) {
                n();
                j();
                return;
            }
        }
        var ad = setTimeout(t, ab);
    };
    this.pauseAnimation = function () {
        a = true;
    };
    var v = function () {
        a = false;
        O = 1;
        I = true;
        for (var ab = 0; ab < p; ab++) {
            u[ab].pauseAnimation();
        }
        for (var ab = 0; ab < aa; ab++) {
            M[ab].pauseAnimation();
        }
        for (var ab = 0; ab < Z; ab++) {
            X[ab].pauseAnimation();
        }
        z.pauseAnimation();
    };
    this.pauseAll = function () {
        r = true;
    };
    var j = function () {
        r = false;
        G = true;
        I = true;
    };
    this.resumeAnimation = function () {
        O = 1;
        I = true;
        for (var ab = 0; ab < p; ab++) {
            u[ab].resumeAnimation();
        }
        for (ab = 0; ab < aa; ab++) {
            M[ab].resumeAnimation();
        }
        for (ab = 0; ab < Z; ab++) {
            X[ab].resumeAnimation();
        }
        z.resumeAnimation();
        if (G == true) {
            G = false;
            t();
        }
    };
    var n = function () {
        $('#overlay').animate({ top: '500' }, { duration: 1500 }, function () {});
    };
    this.setStickman = function (ab) {
        deleteLoader();
        W = ab;
        z.setStickman(W);
        for (var ac = 0; ac < p; ac++) {
            u[ac].setStickman(W);
        }
    };
    this.setKey = function (ab) {
        y = ab;
        W.holdItem(y);
        z.setNextWalkToUnlock(y);
    };
    this.setEgg = function (ab) {
        egg = ab;
        egg.setupEggFall(W.getFeetPosition().y + 45);
        e.setPathGroup(egg);
        setTimeout(V, 300);
    };
    this.setCloud = function (ab) {
        l = ab;
        l.setupRainCloud(W.getPathGroup());
        k = true;
        W.stopHoldingItem(q);
        q.clearDisplay();
        P = 33;
    };
    this.setSword = function (ab) {
        q = ab;
        W.holdItem(q);
        P = 33;
    };
    this.drainDrawn = function (ab) {
        l.endRain();
        S = ab;
        k = false;
        z.setSwirling(true);
        U = true;
    };
    this.swirlingStarted = function () {
        S.clearDisplay();
    };
    var V = function () {
        h = true;
    };
    var D = function (ab) {
        hu = window.location.search.substring(1);
        gy = hu.split('&');
        for (i = 0; i < gy.length; i++) {
            ft = gy[i].split('=');
            if (ft[0] == ab) {
                return ft[1];
            }
        }
    };
    var Q = function () {
        var ae = D('o');
        if (ae != null) {
            var ab = ae.split('s');
            var aj = ab[0];
            var ai = ab[1];
            var ad = aj.split('-');
            var al = ai.split('-');
        } else {
            ad = new Array(66, 69, 32, 67, 82, 69, 65, 84, 73, 86, 69);
            al = new Array(69, 86, 69, 82, 89, 32, 68, 65, 89);
        }
        var af = ad.length;
        g.push('');
        var ah = new Array();
        for (var ag = 0; ag < af; ag++) {
            var ac = String.fromCharCode(ad[ag]).toLowerCase();
            ah.push(ac.toUpperCase());
            if (ac == '?') {
                ac = 'quest';
            } else {
                if (ac == '!') {
                    ac = 'ex';
                } else {
                    if (ac == '.') {
                        ac = 'dot';
                    } else {
                        if (ac == '-') {
                            ac = 'dash';
                        } else {
                            if (ac == ',') {
                                ac = 'com';
                            } else {
                                if (ac == ' ') {
                                    ac = 'blank';
                                } else {
                                    if (ac == '&') {
                                        ac = 'and';
                                    } else {
                                        if (ac == '/') {
                                            ac = 'slash';
                                        } else {
                                            if (ac == '+') {
                                                ac = 'plus';
                                            } else {
                                                if (ac == '@') {
                                                    ac = 'at';
                                                } else {
                                                    if (ac == '"') {
                                                        ac = 'quot';
                                                    } else {
                                                        if (ac == "'") {
                                                            ac = 'apos';
                                                        } else {
                                                            if (ac == '$') {
                                                                ac = 'dol';
                                                            } else {
                                                                if (ac == '%') {
                                                                    ac = 'perc';
                                                                } else {
                                                                    if (ac == '#') {
                                                                        ac = 'hash';
                                                                    } else {
                                                                        if (ac == '>') {
                                                                            ac = 'grat';
                                                                        } else {
                                                                            if (ac == '<') {
                                                                                ac = 'les';
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            g.push(ac);
        }
        $('#firstLine').val(ah.join(''));
        for (ag = af; ag < Y; ag++) {
            g.push('blank');
        }
        af = al.length;
        B.push('');
        var ak = new Array();
        for (ag = 0; ag < af; ag++) {
            ac = String.fromCharCode(al[ag]).toLowerCase();
            ak.push(ac.toUpperCase());
            if (ac == '?') {
                ac = 'quest';
            } else {
                if (ac == '!') {
                    ac = 'ex';
                } else {
                    if (ac == '.') {
                        ac = 'dot';
                    } else {
                        if (ac == '-') {
                            ac = 'dash';
                        } else {
                            if (ac == ',') {
                                ac = 'com';
                            } else {
                                if (ac == ' ') {
                                    ac = 'blank';
                                } else {
                                    if (ac == '&') {
                                        ac = 'and';
                                    } else {
                                        if (ac == '/') {
                                            ac = 'slash';
                                        } else {
                                            if (ac == '+') {
                                                ac = 'plus';
                                            } else {
                                                if (ac == '@') {
                                                    ac = 'at';
                                                } else {
                                                    if (ac == '"') {
                                                        ac = 'quot';
                                                    } else {
                                                        if (ac == "'") {
                                                            ac = 'apos';
                                                        } else {
                                                            if (ac == '$') {
                                                                ac = 'dol';
                                                            } else {
                                                                if (ac == '%') {
                                                                    ac = 'perc';
                                                                } else {
                                                                    if (ac == '#') {
                                                                        ac = 'hash';
                                                                    } else {
                                                                        if (ac == '>') {
                                                                            ac = 'grat';
                                                                        } else {
                                                                            if (ac == '<') {
                                                                                ac = 'les';
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            B.push(ac);
        }
        $('#secLine').val(ak.join(''));
        for (ag = af; ag < w; ag++) {
            B.push('blank');
        }
        setupShareLinks();
    };
    var J = function () {
        var ah = H.clips;
        p = H.numClips;
        Q();
        for (var ac = 0; ac < p; ac++) {
            var am = ah[ac];
            if (am.imageFileName.indexOf('letterb') != -1) {
                var ad = am.imageFileName;
                var af = ad.split('.')[0];
                var ag = af.substr(7, af.length - 7);
                var ai = B[ag];
                am.imageFileName = 'letter' + ai + '.png';
            } else {
                if (am.imageFileName.indexOf('letter') != -1) {
                    var ad = am.imageFileName;
                    var af = ad.split('.')[0];
                    var ag = af.substr(6, af.length - 6);
                    var ai = g[ag];
                    am.imageFileName = 'letter' + ai + '.png';
                }
            }
            var al = new Clip(
                b,
                am.imageFileName,
                am.numFrames,
                am.framesX,
                am.framesY,
                am.framesWidth,
                am.framesHeight,
                am.framesRotate,
                am.framesAlpha,
                am.checkPoints,
                am.framesGoto,
                am.framesResume,
                m,
                am.speechFlag,
                am.animFrames
            );
            u[am.zIndex] = al;
            if (am.imageFileName == 'donebutton.png') {
                R = al;
            } else {
                if (am.imageFileName == 'clearbutton.png') {
                    A = al;
                }
            }
        }
        var ab = H.drawAreas;
        aa = H.numDrawAreas;
        for (var ac = 0; ac < aa; ac++) {
            var am = ab[ac];
            var aj = new DrawArea(b, F, am.frameStart, am.frameEnd, new Rectangle(am.left, am.top, am.right, am.bottom), R, A);
            M.push(aj);
        }
        var ak = H.pathGuides;
        Z = H.numPathGuides;
        for (var ac = 0; ac < Z; ac++) {
            am = ak[ac];
            var ae = new PathGuide(b, am.numFrames, am.framesX, am.framesY, am.framesAlpha, am.animFrames);
            if (am.pathName == 'egg') {
                e = ae;
            }
            X.push(ae);
        }
        am = H.gesture;
        z = new StickmanGuide(
            F,
            am.numFrames,
            am.framesTLX,
            am.framesTLY,
            am.framesTRX,
            am.framesTRY,
            am.framesBLX,
            am.framesBLY,
            am.framesBRX,
            am.framesBRY,
            am.framesWalkX,
            am.framesWalkY,
            am.framesX,
            am.framesY,
            am.framesScaleX,
            am.framesScaleY
        );
        if (M[1]) {
            M[1].drawAsKey();
            M[1].setDrawColor('#e58342');
        }
        if (M[2]) {
            M[2].drawAsEgg();
            M[2].setDrawColor('#fd5b2a');
        }
        if (M[3]) {
            M[3].drawAsSword();
            M[3].setDrawColor('#9f9f9f');
        }
        if (M[4]) {
            M[4].drawAsCloud();
            M[4].setDrawColor('#888888');
        }
        if (M[5]) {
            M[5].drawAsDrain();
            M[5].setDrawColor('#8c8f91');
        }
        for (ac = 0; ac < p; ac++) {
            u[ac] && u[ac].loadImage();
        }
    };
    this.loadAnimation = function () {
        $.ajax({
            url: 'js/animguides.json',
            cache: true,
            success: function (ab) {
                H = ab;
                J();
            },
        });
    };
}
