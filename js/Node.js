function Node(h, g, f, d) {
    var b = h;
    var a = g;
    var i = f;
    var c = d;
    var e = false;
    this.getX = function () {
        return b;
    };
    this.getY = function () {
        return a;
    };
    this.isExtraNode = function () {
        return c;
    };
    this.markForDelete = function () {
        e = true;
    };
    this.isMarkedForDelete = function () {
        return e;
    };
    this.move = function (j, k) {
        b += j;
        a += k;
    };
}
