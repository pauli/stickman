function Animation(a) {
    var e = new Array();
    var d = 0;
    var g = 0;
    var c = 0;
    var h = 0;
    var b = new Array();
    var f = a;
    this.setPropFunction = function (i) {
        f = i;
    };
    this.addTween = function (i) {
        e.push(i);
    };
    this.animate = function () {
        h++;
        var i = e[d].animate(f);
        if (b[h] != null) {
            b[h]();
        }
        if (i == false) {
            d++;
            if (d >= e.length) {
                d = g;
                h = c;
            }
        }
    };
    this.setLoop = function (j) {
        g = j;
        c = 0;
        for (var k = 0; k < g; k++) {
            c += e[k].getNumFrames() - 1;
        }
    };
    this.callFunctionAt = function (j, i) {
        b[i] = j;
    };
}
