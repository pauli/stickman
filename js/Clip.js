function Clip(d, r, O, M, B, F, D, p, v, n, a, I, l, q, N) {
    var b = d;
    var f = r;
    var P = 100;
    var G = 100;
    var g;
    var i = M;
    var h = B;
    var u = F;
    var c = D;
    var z = p;
    var o = v;
    var e = n;
    var A = a;
    var k = I;
    var m = O;
    var w = 1;
    var x;
    var E = l;
    var L = false;
    var s = false;
    var K = q;
    var J;
    var C;
    var t = N;
    var H = 0;
    var y = 0;
    var j = function () {
        E();
    };
    this.loadImage = function () {
        g = b.image('img/' + f, i[0], h[0], u[0], c[0]);
        x = new Image();
        x.src = 'img/' + f;
        x.onLoad = j();
    };
    this.animateFrame = function (X) {
        var Y = true;
        for (var T = 0; T < X; T++) {
            if (H > m || (L == true && s == false)) {
            } else {
                if (t[H] != true) {
                    H++;
                    if (L == false) {
                        y++;
                    }
                } else {
                    if (i[H] != null) {
                        var Q = i[H];
                    }
                    if (h[H] != null) {
                        var Z = h[H];
                    }
                    if (u[H] != null) {
                        var S = u[H];
                    }
                    if (c[H] != null) {
                        var R = c[H];
                    }
                    if (o[H] != null) {
                        var W = o[H];
                    }
                    if (A[H] != null) {
                        var V = A[H];
                    }
                    if (z[H] != null) {
                        var U = z[H];
                    }
                    H++;
                    if (L == false) {
                        y++;
                    }
                    if (V != null) {
                        s = true;
                        H = e[V];
                        V = null;
                    }
                    if (k[y] != null) {
                        s = false;
                        H = y;
                    }
                    Y = false;
                }
            }
        }
        if (Y == true) {
            return;
        }
        if (R != null) {
            C = R;
        }
        if (Z != null && K == true && J != null) {
            Z = J.getTopPos() - C - 10;
        }
        if (Q != null && Z != null && S != null && R != null) {
            g.attr({ x: Q, y: Z, width: S, height: R });
        } else {
            if (Q != null && Z != null && S != null) {
                g.attr({ x: Q, y: Z, width: S });
            } else {
                if (Q != null && Z != null && R != null) {
                    g.attr({ x: Q, y: Z, height: R });
                } else {
                    if (Q != null && S != null && R != null) {
                        g.attr({ x: Q, width: S, height: R });
                    } else {
                        if (Z != null && S != null && R != null) {
                            g.attr({ y: Z, width: S, height: R });
                        } else {
                            if (Q != null && Z != null) {
                                g.attr({ x: Q, y: Z });
                            } else {
                                if (S != null && R != null) {
                                    g.attr({ width: S, height: R });
                                } else {
                                    if (Q != null && R != null) {
                                        g.attr({ x: Q, height: R });
                                    } else {
                                        if (Q != null && S != null) {
                                            g.attr({ x: Q, width: S });
                                        } else {
                                            if (Z != null && S != null) {
                                                g.attr({ y: Z, width: S });
                                            } else {
                                                if (Z != null && R != null) {
                                                    g.attr({ y: Z, height: R });
                                                } else {
                                                    if (Q != null) {
                                                        g.attr({ x: Q });
                                                    } else {
                                                        if (Z != null) {
                                                            g.attr({ y: Z });
                                                        } else {
                                                            if (S != null) {
                                                                g.attr({ width: S });
                                                            } else {
                                                                if (R != null) {
                                                                    g.attr({ height: R });
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (U != null) {
            g.rotate(U, g.attr('x'), g.attr('y'));
        }
        if (W != null && W != w) {
            if (W == 0) {
                g.hide();
            } else {
                g.show();
            }
            w = W;
        }
    };
    this.getImage = function () {
        return g;
    };
    this.pauseAnimation = function () {
        L = true;
    };
    this.resumeAnimation = function () {
        L = false;
    };
    this.setStickman = function (Q) {
        J = Q;
    };
}
