function Rectangle(b, c, d, a) {
    this.left = b;
    this.top = c;
    this.right = d;
    this.bottom = a;
    this.addPoint = function (f, e) {
        if (f < this.left) {
            this.left = f;
        }
        if (e < this.top) {
            this.top = e;
        }
        if (f > this.right) {
            this.right = f;
        }
        if (e > this.bottom) {
            this.bottom = e;
        }
    };
    this.addRectangle = function (e) {
        this.addPoint(e.left, e.top);
        this.addPoint(e.right, e.bottom);
    };
    this.getCenterX = function () {
        return this.left + (this.right - this.left) * 0.5;
    };
    this.getCenterY = function () {
        return this.top + (this.bottom - this.top) * 0.5;
    };
    this.isPointWithinRect = function (f, e) {
        if (f >= this.left && f <= this.right && e >= this.top && e <= this.bottom) {
            return true;
        }
        return false;
    };
}
