function Line(a) {
    var c = a;
    var b = a;
    this.isNearLine = function (e, d) {
        if (e >= c - d && e <= b + d) {
            return true;
        }
        return false;
    };
    this.addPoint = function (d) {
        if (d > b) {
            b = d;
        } else {
            if (d < c) {
                c = d;
            }
        }
    };
    this.getLength = function () {
        return Math.abs(b - c);
    };
}
