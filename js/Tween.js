function Tween(h, f, c) {
    var e = h;
    var d = f;
    var b;
    var a = c;
    var g = 0;
    this.animate = function (l) {
        if (g == 0) {
            g++;
            b = l();
        }
        var k = (d + 1) * 0.5;
        var m = (a - b) * 0.5;
        if (g < k) {
            var j = Math.sin((g / k) * (Math.PI / 2));
            var i = b + m * (1 - Math.sin((1 - g / k) * (Math.PI / 2)));
        } else {
            i = b + m + m * Math.sin(((g - k) / k) * (Math.PI / 2));
        }
        e(i);
        g++;
        if (g > d + 1) {
            g = 0;
            return false;
        }
        return true;
    };
    this.getNumFrames = function () {
        return d;
    };
}
