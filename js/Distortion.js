function Distortion(i) {
    var j = i;
    var f = j.left;
    var d = j.top;
    var b = j.right;
    var a = j.top;
    var h = j.left;
    var g = j.bottom;
    var e = j.right;
    var c = j.bottom;
    this.setDistort = function (q, o, r, p, m, k, n, l) {
        f = q;
        d = o;
        b = r;
        a = p;
        h = m;
        g = k;
        e = n;
        c = l;
    };
    this.scaleTLx = function (k) {
        f = j.left + (j.right - j.left) * (1 - k);
    };
    this.scaleTRx = function (k) {
        b = (j.right - j.left) * k + j.left;
    };
    this.scaleBLx = function (k) {
        h = (j.right - j.left) * (1 - k) + j.left;
    };
    this.scaleBRx = function (k) {
        e = (j.right - j.left) * k + j.left;
    };
    this.scaleTLy = function (m) {
        var l = d;
        d = j.top + (j.bottom - j.top) * (1 - m);
        var k = d;
    };
    this.scaleTRy = function (k) {
        a = j.top + (j.bottom - j.top) * (1 - k);
    };
    this.scaleBLy = function (k) {
        g = (j.bottom - j.top) * k + j.top;
    };
    this.scaleBRy = function (k) {
        c = (j.bottom - j.top) * k + j.top;
    };
    this.getTLScaleX = function () {
        return (j.right - f) / (j.right - j.left);
    };
    this.getTRScaleX = function () {
        return (b - j.left) / (j.right - j.left);
    };
    this.getBLScaleX = function () {
        return (j.right - h) / (j.right - j.left);
    };
    this.getBRScaleX = function () {
        return (e - j.left) / (j.right - j.left);
    };
    this.getTLScaleY = function () {
        return (j.bottom - d) / (j.bottom - j.top);
    };
    this.getTRScaleY = function () {
        return (j.bottom - a) / (j.bottom - j.top);
    };
    this.getBLScaleY = function () {
        return (g - j.top) / (j.bottom - j.top);
    };
    this.getBRScaleY = function () {
        return (c - j.top) / (j.bottom - j.top);
    };
    this.distortPointX = function (q, p) {
        var k = (p - j.top) / (j.bottom - j.top);
        var o = (h - f) * k;
        var l = b - f + (e - h - (b - f)) * k;
        var n = l / (j.right - j.left);
        var m = j.left + (q - j.left) * n + o;
        return m;
    };
    this.distortPointY = function (q, p) {
        var l = (q - j.left) / (j.right - j.left);
        var o = (a - d) * l;
        var k = g - d + (c - a - (g - d)) * l;
        var n = k / (j.bottom - j.top);
        var m = j.top + (p - j.top) * n + o;
        return m;
    };
}
