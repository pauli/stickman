function DrawArea(e, X, w, x, J, m, E) {
    var c = e;
    var z = X;
    var Y = w;
    var b = x;
    var W = J;
    var T = false;
    var O = m;
    var s = false;
    var v = E;
    var H = 0;
    var a = 900;
    var F = 900;
    var D;
    var f;
    var u;
    var S = new Array();
    var B = new Array();
    var l;
    var k;
    var M = '';
    var U = false;
    var G = false;
    var R = false;
    var d = false;
    var V = false;
    var C = false;
    var y = '#555';
    this.setXMLAnim = function (Z) {
        z = Z;
    };
    this.animateFrame = function (Z) {
        if (T == true) {
            return;
        }
        for (var aa = 0; aa < Z; aa++) {
            if (H == Y) {
                o();
            } else {
                if (H == b) {
                    z.pauseAll();
                    H++;
                    return;
                }
            }
            H++;
            if (H >= Y && H <= b) {
                return aa + 1;
            }
        }
        return Z;
    };
    this.pauseAnimation = function () {
        T = true;
        var Z = O.getImage();
    };
    this.resumeAnimation = function () {
        if (H < b) {
            T = false;
        }
    };
    this.drawAsKey = function () {
        G = true;
    };
    this.drawAsEgg = function () {
        R = true;
    };
    this.drawAsCloud = function () {
        V = true;
    };
    this.drawAsSword = function () {
        d = true;
    };
    this.drawAsDrain = function () {
        C = true;
    };
    this.setDrawColor = function (Z) {
        y = Z;
    };
    var L = function (Z) {
        if (Z.pageX) {
            return Z.pageX;
        } else {
            if (Z.clientX) {
                return Z.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
            } else {
                return null;
            }
        }
    };
    var K = function (Z) {
        if (Z.pageY) {
            return Z.pageY;
        } else {
            if (Z.clientY) {
                return Z.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
            } else {
                return null;
            }
        }
    };
    var I = function () {
        if (u.findFeet() == true) {
            var Z = new Stickman(u, c);
            z.setStickman(Z);
        }
        n();
        z.resumeAnimation();
    };
    var n = function () {
        D.remove();
        try {
            document.removeEventListener('touchstart', g, false);
            document.removeEventListener('touchmove', g, false);
            document.removeEventListener('touchend', g, false);
            document.removeEventListener('touchcancel', g, false);
        } catch (Z) {}
    };
    var P = function (aa, Z) {
        var ac = l + aa;
        var ab = k + Z;
        mousePosX = ac;
        mousePosY = ab;
        t(ac, ab);
    };
    var t = function (aa, Z) {
        if (W.isPointWithinRect(aa, Z) == true) {
            f.quickDrawNode(aa, Z);
        }
    };
    var j = function (ab) {
        f = new Path(c, y);
        if (s == true) {
            s = false;
            z.resumeAnimation();
            var Z = O.getImage();
            Z.click(p);
            Z.mouseover(Q);
            Z.mouseout(A);
            var aa = v.getImage();
            aa.click(r);
            aa.mouseover(Q);
            aa.mouseout(A);
        }
    };
    var r = function (Z) {
        u.clearDisplay();
    };
    var p = function (ab) {
        var Z = O.getImage();
        Z.unclick(p);
        Z.unmouseover(Q);
        Z.unmouseout(A);
        var aa = v.getImage();
        aa.unclick(r);
        aa.unmouseover(Q);
        aa.unmouseout(A);
        document.body.style.cursor = 'default';
        if (G == true) {
            z.setKey(u);
            n();
            z.resumeAnimation();
        } else {
            if (R == true) {
                z.setEgg(u);
                n();
                z.resumeAnimation();
            } else {
                if (d == true) {
                    z.setSword(u);
                    n();
                    z.resumeAnimation();
                } else {
                    if (V == true) {
                        z.setCloud(u);
                        n();
                        z.resumeAnimation();
                    } else {
                        if (C == true) {
                            z.drainDrawn(u);
                            n();
                            z.resumeAnimation();
                        } else {
                            I();
                        }
                    }
                }
            }
        }
    };
    var Q = function (Z) {
        document.body.style.cursor = 'pointer';
    };
    var A = function (Z) {
        document.body.style.cursor = 'default';
    };
    var N = function (ab) {
        if (f == null) {
            return;
        }
        if (f.getNumNodes() > 0) {
            f.finalizePath();
            u.addPath(f);
            if (D != null) {
                D.toFront();
                var Z = O.getImage();
                Z.toFront();
                var aa = v.getImage();
                aa.toFront();
            }
            f = new Path(c, y);
        }
    };
    var g = function (ad) {
        if (ad.type == 'touchstart') {
            mousePosX = ad.touches[0].clientX;
            mousePosY = ad.touches[0].clientY;
            var ab = O.getImage();
            var aa = ab.getBBox();
            var ac = v.getImage();
            var Z = ac.getBBox();
            var ae = $('#holder').offset();
            if (mousePosX - ae.left >= aa.x && mousePosX - ae.left <= aa.x + aa.width && mousePosY - ae.top >= aa.y && mousePosY - ae.top <= aa.y + aa.height) {
                p();
            } else {
                if (mousePosX - ae.left >= Z.x && mousePosX - ae.left <= Z.x + Z.width && mousePosY - ae.top >= Z.y && mousePosY - ae.top <= Z.y + Z.height) {
                    r();
                } else {
                    if (W.isPointWithinRect(mousePosX, mousePosY) == true) {
                        N();
                        j();
                        t(ad.touches[0].clientX, ad.touches[0].clientY);
                        U = true;
                        ad.preventDefault();
                    } else {
                        U = false;
                    }
                }
            }
        } else {
            if (ad.type == 'touchmove') {
                if (U == true) {
                    t(ad.touches[0].clientX, ad.touches[0].clientY);
                    ad.preventDefault();
                } else {
                }
            } else {
                if (ad.type == 'touchend' || ad.type == 'touchcancel') {
                    if (U == true) {
                        N();
                        ad.preventDefault();
                    }
                }
            }
        }
    };
    var o = function () {
        z.pauseAll();
        s = true;
        u = new PathGroup(c);
        var Z = navigator.userAgent.match(/iPad/i) != null;
        D = c.rect(0, 0, c.width, c.height, 0).attr({ fill: '#f00', opacity: 0 });
        if (Z == false) {
            D.drag(P, j, N);
            document.onmousedown = i;
        }
        try {
            document.addEventListener('touchstart', g, false);
            document.addEventListener('touchmove', g, false);
            document.addEventListener('touchend', g, false);
            document.addEventListener('touchcancel', g, false);
        } catch (aa) {}
    };
    function q(Z) {
        Z = Z || window.event;
        l = L(Z);
        k = K(Z);
    }
    function i(Z) {
        Z = Z || window.event;
        var aa = $('#holder').offset();
        l = L(Z) - aa.left;
        k = K(Z) - aa.top;
    }
    function h(Z) {
        document.onmousemove = null;
        document.onmouseup = null;
    }
}
